import sys
from time import sleep

import pygame
from pygame.sprite import Group
import json
from random import randint

from bullet import Bullet
from alien import Alien
from stars import Star

def check_events(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets):
    """Respond to keypresses and mouse events."""
    for event in pygame.event.get():

        #Quitting game
        if event.type == pygame.QUIT:
            sys.exit()

        #Ship movement
        elif event.type == pygame.KEYDOWN:
            mouse_x,mouse_y = pygame.mouse.get_pos()
            check_keydown_events(
                event,ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,
                mouse_x,mouse_y)

        elif event.type == pygame.KEYUP:
            check_keyup_events(event,ship)
        
        #Play button
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x,mouse_y = pygame.mouse.get_pos()
            check_play_button(
                ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y)

def check_play_button(
    ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y):
    """Start a new game when the player clicks Play."""
    button_clicked = play_button.rect.collidepoint(mouse_x,mouse_y)
    if button_clicked and not stats.game_active:
        start_game(
            ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y)

def start_game(
    ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y):
    """Start the game."""
    #Reset game settings.
    ai_settings.initialize_dynamic_settings()

    #Reset game stats.
    stats.reset_stats()

    #Reset the scoreboard images.
    sb.prep_score()
    sb.prep_high_score()
    sb.prep_level()
    sb.prep_ships()

    #Empty list of aliens and bullets.
    aliens.empty()
    bullets.empty()

    #Create new fleet and center ship.
    create_fleet(ai_settings,screen,ship,aliens)
    ship.center_ship()

    #Hide mouse.
    pygame.mouse.set_visible(False)
    if play_button.rect.collidepoint(mouse_x,mouse_y):
        stats.game_active = True



def check_keydown_events(
    event,ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y):
    """Respond to keypresses."""
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    elif event.key == pygame.K_LEFT:
        ship.moving_left = True
    elif event.key == pygame.K_UP:
        ship.moving_up = True
    elif event.key == pygame.K_DOWN:
        ship.moving_down = True
    elif not stats.game_active and event.key == pygame.K_p:
        start_game(
            ai_settings,screen,stats,sb,play_button,ship,aliens,bullets,mouse_x,mouse_y)
    elif event.key == pygame.K_SPACE:
        fire_bullet(ai_settings,screen,ship,bullets)
    elif event.key == pygame.K_q:
        sys.exit()

def check_keyup_events(event,ship):
    """Respond to key releases"""
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    elif event.key == pygame.K_LEFT:
        ship.moving_left = False
    elif event.key == pygame.K_UP:
        ship.moving_up = False
    elif event.key == pygame.K_DOWN:
        ship.moving_down = False

def fire_bullet(ai_settings,screen,ship,bullets):
    """Fire a bullet if limit not reached yet."""
    #Create new bullet and add it to the bullets group.
    if len(bullets) < ai_settings.bullets_allowed:
        new_bullet = Bullet(ai_settings,screen,ship)
        bullets.add(new_bullet)

def update_bullets(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Update position of bullets and get rid of old bullets."""
    #Update bullet positions.
    bullets.update()

    #Get rid of bullets that have disapperead.
    for bullet in bullets.copy():
        if bullet.rect.bottom <= 0:
            bullets.remove(bullet)

    check_bullet_alien_collisions(ai_settings,screen,stats,sb,ship,aliens,bullets)

def check_bullet_alien_collisions(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Respond to bullet-alien collisions."""
    #Remove bullets and aliens that have collided.
    #Compares rects of bullets and aliens and deletes if collided.
    collisions = pygame.sprite.groupcollide(bullets,aliens,True,True)
    #Increase score
    if collisions:
        for aliens in collisions.values():
            stats.score += ai_settings.alien_points * len(aliens)
            sb.prep_score()
        check_high_score(stats,sb)

    #Destroy existing bullets, speed up game, create new fleet, and start new level.
    if len(aliens) == 0:
        start_new_level(ai_settings,screen,stats,sb,ship,aliens,bullets)

def start_new_level(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Start new level when all aliens dead."""
    ship.center_ship()
    bullets.empty()
    ai_settings.increase_speed()
    stats.level += 1
    sb.prep_level()
    create_fleet(ai_settings,screen,ship,aliens)

def update_screen(ai_settings,screen,stats,sb,ship,aliens,bullets,play_button,stars):
    """Update images on the screen and flip to the new screen."""
    #Redraw the screen, each pass through the loop.
    screen.fill(ai_settings.bg_color)

    #Draw the stars
    i = 0
    while i < 5:
        stars.draw(screen)
        stars.update()
        i += 1

    #Redraw all bullets
    for bullet in bullets.sprites():
        bullet.draw_bullet()
    ship.blitme()
    aliens.draw(screen)

    #Draw the socreboard.
    sb.show_score()

    #Draw the play button if the game is inactive.
    if not stats.game_active:
        play_button.draw_button()

    #Make the most recently drawn screen visible.
    pygame.display.flip()

def get_number_aliens_x(ai_settings,alien_width):
    """Determine the number of aliens that fit in a row."""
    available_space_x = ai_settings.screen_width - 2 * alien_width
    number_aliens_x = int(available_space_x / (2 *alien_width))
    return number_aliens_x

def get_number_rows(ai_settings,ship_height,alien_height):
    """Determine the number of rows of aliens that fit on the screen."""
    available_space_y = (ai_settings.screen_height -
                         (3 * alien_height) - ship_height)
    number_rows = int(available_space_y / (2 * alien_height))
    return number_rows

def create_alien(ai_settings,screen,aliens,alien_number,row_number):
    """Create an alien and place it in the row."""
    alien = Alien(ai_settings,screen)
    alien_width = alien.rect.width
    alien.x = alien_width + 2 * alien_width * alien_number
    alien.rect.x = alien.x
    alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
    aliens.add(alien)

def create_fleet(ai_settings,screen,ship,aliens):
    """Create a full fleet of aliens."""
    #Create an alien and find the number of aliens in a row.
    alien = Alien(ai_settings,screen)
    number_aliens_x = get_number_aliens_x(ai_settings,alien.rect.width)
    number_rows = get_number_rows(ai_settings,ship.rect.height,alien.rect.height)

    #Create the first row of aliens.
    for row_number in range(number_rows):
        for alien_number in range(number_aliens_x):
            create_alien(ai_settings,screen,aliens,alien_number,row_number)

def create_stars(screen,stars):
    """Create screen of stars."""
    #Create star
    star = Star(screen)
    num = 0
    while num < 10:
        stars.add(star)
        stars.update()
        num += 1

def check_fleet_edges(ai_settings,aliens):
    """Respond appropriately if any aliens have reached an edge."""
    for alien in aliens.sprites():
        if alien.check_edges():
            change_fleet_direction(ai_settings,aliens)
            break

def change_fleet_direction(ai_settings,aliens):
    """Drop the entire fleet and change the fleet's direction."""
    for alien in aliens.sprites():
        alien.rect.y += ai_settings.fleet_drop_speed
    ai_settings.fleet_direction *= -1

def ship_hit(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Respond to ship being hit by alien."""
    #Decrement ships_left.
    if len(aliens) < 36:
        if stats.ships_left > 0:
            stats.ships_left -= 1

            #Update scoreborad.
            sb.prep_ships()

            #Empty the list of aliens and bullets.
            aliens.empty()
            bullets.empty()

            #Create a new fleet and center the ship.
            create_fleet(ai_settings,screen,ship,aliens)
            ship.center_ship()

            #Pause.
            sleep(0.5)
        else:
            stats.game_active = False
            pygame.mouse.set_visible(True)

def update_aliens(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Check if aliens hit edge and update the positions of all aliens in the fleet."""
    check_fleet_edges(ai_settings,aliens)
    aliens.update()

    #Look for alien_ship collisions
    if pygame.sprite.spritecollideany(ship,aliens):
        ship_hit(ai_settings,screen,stats,sb,ship,aliens,bullets)
    
    #Look for aliens hitting bottom.
    check_aliens_bottom(ai_settings,screen,stats,sb,ship,aliens,bullets)

def check_aliens_bottom(ai_settings,screen,stats,sb,ship,aliens,bullets):
    """Check if any aliens have reached the bottom of the screen."""
    screen_rect = screen.get_rect()
    for alien in aliens.sprites():
        if alien.rect.bottom >= screen_rect.bottom:
            #Treat this same as ship got hit.
            ship_hit(ai_settings,screen,stats,sb,ship,aliens,bullets)
            break

def check_high_score(stats,sb):
    """Check to see if there's a new high score."""
    filename = 'highscore.json'
    with open(filename,'r+') as f:
        if stats.score > stats.high_score:
            stats.high_score = stats.score
            json.dump(stats.high_score,f)
            sb.prep_high_score()
