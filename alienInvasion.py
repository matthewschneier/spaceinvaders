import pygame
from pygame.sprite import Group

from settings import Settings
from ship import Ship
from alien import Alien
from stats import Stats
from button import Button
from scoreboard import Scoreboard
from stars import Star
import gameFunctions as gf

def run_game():
    """Initialize game and create a screen object."""
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))

    #Box caption
    pygame.display.set_caption('Alien Invasion!')

    #Make the Play button.
    play_button = Button(ai_settings,screen,'Play')

    #Create instance to store game stats and scoreboard
    stats = Stats(ai_settings)
    sb = Scoreboard(ai_settings,screen,stats)

    #Set background color.
    bg_color = (175,175,175)

    #Make a ship.
    ship = Ship(ai_settings,screen)
    
    #Make aliens.
    aliens = Group()
    gf.create_fleet(ai_settings,screen,ship,aliens)

    #Make bullets group.
    bullets = Group()

    #Make stars.
    stars = Group()
    gf.create_stars(screen,stars)

    #Make an alien.
    alien = Alien(ai_settings,screen)

    #Start the main loop for game.
    while True:
        gf.check_events(ai_settings,screen,stats,sb,play_button,ship,aliens,bullets)
        if stats.game_active:
            ship.update()
            gf.update_bullets(ai_settings,screen,stats,sb,ship,aliens,bullets)
            gf.update_aliens(ai_settings,screen,stats,sb,ship,aliens,bullets)
        gf.update_screen(ai_settings,screen,stats,sb,ship,aliens,bullets,play_button,stars)

run_game()
