from random import randint

import pygame
from pygame.sprite import Sprite

class Star(Sprite):
    """A class to represent a single background star."""

    def __init__(self,screen):
        """Initialize star and position."""
        super(Star,self).__init__()
        self.screen = screen

        #Load the star image and set its rect attribute.
        self.image = pygame.image.load('images/star.bmp')
        self.rect = self.image.get_rect()

        #Start each new star at top left of screen.
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        #Store the alien's exact position.
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

    def blitme(self):
        """Draw star at current location."""
        self.screen.blit(self.image,self.rect)

    def update(self):
        """Move star randomly."""
        self.x += randint(-200,200)
        self.y += randint(-200,200)
